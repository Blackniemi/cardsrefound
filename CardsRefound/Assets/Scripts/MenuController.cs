﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuController : MonoBehaviour {

    AudioSource audioPlayer;
    AudioClip sfx_characters, sfx_button_click;
    List<AudioClip> sfx_roles;

    CanvasGroup menuCG;

    public int playerCount;
    public int turnCount;
    public string turnCountStr;
    public Text playerCountText;
    public Text turnCountText;

    public float menuFadeRate;
    public bool menuFinished;
    bool starting;

    // Use this for initialization
    void Start () {

        audioPlayer = GameObject.Find("MASTER").GetComponent<AudioSource>();

        menuCG = GetComponent<CanvasGroup>();

        sfx_button_click = Resources.Load<AudioClip>("Audio/Effects/Button_click");

        sfx_roles = new List<AudioClip>();
        sfx_roles.Add(Resources.Load<AudioClip>("Audio/Roles/pomo_selostus"));
        sfx_roles.Add(Resources.Load<AudioClip>("Audio/Roles/alainen_selostus"));
        sfx_roles.Add(Resources.Load<AudioClip>("Audio/Roles/soluttautuja_selostus"));
        sfx_roles.Add(Resources.Load<AudioClip>("Audio/Roles/hakkeri_selostus"));
        sfx_roles.Add(Resources.Load<AudioClip>("Audio/Roles/murtautuja_selostus"));

        playerCount = 4;
        turnCountStr = "NORMAL";

        playerCountText = GameObject.Find("Text_player_count").GetComponent<Text>();
        playerCountText.text = playerCount.ToString();
        turnCountText = GameObject.Find("Text_turn_count").GetComponent<Text>();
        turnCountText.text = turnCountStr;

        starting = false;
    }
	
	// Update is called once per frame
	void Update () {

        if (starting)
        {
            float a = menuCG.alpha;

            if (a <= 0f)
                menuFinished = true;

            menuCG.alpha = a - menuFadeRate * Time.deltaTime;
        }
		
	}

    IEnumerator CharacterInfoRoutine()
    {
        for (int i = 0; i < 5; i++)
        {
            audioPlayer.clip = sfx_roles[i];
            audioPlayer.Play();

            yield return new WaitForSeconds(audioPlayer.clip.length + 0.5f);
        }
    }

    public void PlayRules()
    {
        if (audioPlayer.isPlaying)
        {
            StopAllCoroutines();
            audioPlayer.Stop();
        }
        else
        {
            StartCoroutine(CharacterInfoRoutine());
        }
    }

    public void AddPlayer()
    {
        if (playerCount < 8)
            playerCount++;

        playerCountText.text = playerCount.ToString();
        audioPlayer.PlayOneShot(sfx_button_click);
    }

    public void RemovePlayer()
    {
        if (playerCount > 4)
            playerCount--;

        playerCountText.text = playerCount.ToString();
        audioPlayer.PlayOneShot(sfx_button_click);
    }

    public void AddTurns()
    {
        if (turnCountStr == "EASY")
            turnCountStr = "NORMAL";
        else if (turnCountStr == "NORMAL")
            turnCountStr = "HARD";

        turnCountText.text = turnCountStr;
        audioPlayer.PlayOneShot(sfx_button_click);
    }

    public void RemoveTurns()
    {
        if (turnCountStr == "NORMAL")
            turnCountStr = "EASY";
        else if (turnCountStr == "HARD")
            turnCountStr = "NORMAL";

        turnCountText.text = turnCountStr;
        audioPlayer.PlayOneShot(sfx_button_click);
    }

    public void StartPressed()
    {
        StopAllCoroutines();
        audioPlayer.Stop();
        CountTurns();
        starting = true;
        audioPlayer.PlayOneShot(sfx_button_click);
    }

    void CountTurns()
    {
        if (turnCountStr == "EASY")
        {
            turnCount = 3 * playerCount - 4;
        }

        if (turnCountStr == "NORMAL")
        {
            turnCount = 2 * playerCount - 3;
        }

        if (turnCountStr == "HARD")
        {
            turnCount = 2 * playerCount - 5;
        }
    }

    public void HideMenu()
    {
        menuCG.alpha = 0f;
        menuCG.gameObject.SetActive(false);
    }
}
