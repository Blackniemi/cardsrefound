﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuessScript : MonoBehaviour {

    public struct Character
    {
        public string name;
        public int num;
        public AudioClip audio;

        public Character(string _name, int _num, AudioClip _audio)
        {
            name = _name;
            num = _num;
            audio = _audio;
        }
    };

    public enum GuessState
    {
        IDLE,
        INTRO,
        SAY_ROLE,
        RECEIVE_ROLE
    };

    Character boss;
    Character lackey;
    Character infiltrator;
    Character hacker;
    Character invader;

    CanvasGroup introCG;

    GuessState guessState;

    AudioSource audioPlayer;
    AudioClip sfx_truth_moment;

    public List<string> guesses;
    public List<string> gameRolePool;
    int currentRole;

    public bool introDone;
    public bool guessingDone;
    public bool guessedRight;
    bool guessReceived;

    // Use this for initialization
    void Start () {

        audioPlayer = GetComponent<AudioSource>();
        sfx_truth_moment = Resources.Load<AudioClip>("Audio/Guessing/totuuden_hetki");
        currentRole = 0;
        introDone = false;

        introCG = GameObject.Find("Panel_intro").GetComponent<CanvasGroup>();

        boss = new Character("boss", 1, Resources.Load<AudioClip>("Audio/Roles/pomo"));
        lackey = new Character("lackey", 2, Resources.Load<AudioClip>("Audio/Roles/alainen"));
        infiltrator = new Character("infiltrator", 3, Resources.Load<AudioClip>("Audio/Roles/soluttautuja"));
        hacker = new Character("hacker", 4, Resources.Load<AudioClip>("Audio/Roles/hakkeri"));
        invader = new Character("invader", 5, Resources.Load<AudioClip>("Audio/Roles/murtautuja"));

        guesses = new List<string>();
    }
	
	// Update is called once per frame
	void Update () {
		
        switch (guessState)
        {
            case GuessState.IDLE:
                break;

            case GuessState.INTRO:

                if (introDone)
                {
                    introCG.alpha = 0f;
                    introCG.interactable = false;
                    introCG.gameObject.SetActive(false);
                    SetState(GuessState.SAY_ROLE);
                }

                break;

            case GuessState.SAY_ROLE:

                break;

            case GuessState.RECEIVE_ROLE:

                if (guessReceived)
                {
                    currentRole++;

                    if (currentRole < gameRolePool.Count)
                    {
                        SetState(GuessState.SAY_ROLE);
                    }
                    else
                    {
                        CheckGuesses();
                    }
                }

                break;
        }
	}

    void CheckGuesses()
    {
        guessedRight = true;

        for (int i=0; i<gameRolePool.Count; i++)
        {
            Debug.Log("ROLE: " + gameRolePool[i] + "    GUESS: " + guesses[i]);
            if (guesses[i] != gameRolePool[i])
            {
                guessedRight = false;
            }
        }

        guessingDone = true;
        guessState = GuessState.IDLE;
    }

    void SetState(GuessState newState)
    {
        switch (newState)
        {
            case GuessState.IDLE:
                break;

            case GuessState.INTRO:
                break;

            case GuessState.SAY_ROLE:

                StartCoroutine(SayRole(gameRolePool[currentRole]));

                break;

            case GuessState.RECEIVE_ROLE:

                guessReceived = false;

                break;
        }

        guessState = newState;
    }

    IEnumerator SayRole(string character_name)
    {
        if (character_name == "boss")
            audioPlayer.clip = boss.audio;
        if (character_name == "lackey")
            audioPlayer.clip = lackey.audio;
        if (character_name == "infiltrator")
            audioPlayer.clip = infiltrator.audio;
        if (character_name == "hacker")
            audioPlayer.clip = hacker.audio;
        if (character_name == "invader")
            audioPlayer.clip = invader.audio;

        audioPlayer.Play();

        yield return new WaitForSeconds(1f);
        yield return new WaitForSeconds(audioPlayer.clip.length);
        SetState(GuessState.RECEIVE_ROLE);
    }

    IEnumerator SayGuessingIntro()
    {
        yield return new WaitForSeconds(3f);

        introCG.alpha = 1f;
        introCG.interactable = true;

        audioPlayer.clip = sfx_truth_moment;
        audioPlayer.Play();

        yield return new WaitForSeconds(audioPlayer.clip.length + 1f);

        introDone = true;
    }

    public void SkipTruth()
    {
        if (guessState == GuessState.INTRO)
        {
            StopAllCoroutines();
            audioPlayer.Stop();
            introDone = true;
        }
    }

    public void StartGuessing(List<string> rolePool)
    {
        gameRolePool = rolePool;
        guessState = GuessState.INTRO;
        introCG.gameObject.SetActive(true);
        StartCoroutine(SayGuessingIntro());
    }

    public void ReceiveGuessRead(int sector, int block, string character)
    {
        guesses.Add(character);
        guessReceived = true;
    }
}
