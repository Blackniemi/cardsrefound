﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardController : MonoBehaviour {

    CanvasGroup cardsCG;

    public Transform eye;
    public Transform card1, card2, card3;

    Renderer eyeMat1, eyeMat2;

    Vector3 card1_pos_1 = new Vector3(-500f, 0f, 0f);
    Vector3 card1_pos_2 = new Vector3(-650f, 0f, 0f);
    Vector3 card1_pos_3 = new Vector3(-800f, 0f, 0f);

    Vector3 card2_pos_2 = new Vector3(-350f, 0f, 0f);
    Vector3 card2_pos_3 = new Vector3(-500f, 0f, 0f);

    Vector3 card3_pos_3 = new Vector3(-200f, 0f, 0f);

    Vector3 card_pos_offscreen = new Vector3(-5000f, -5000f, 0f);

    public float cardsRevealRate;
    public float eyeRevealRate;

    public bool revealing;
    public float revealDelayTimer;
    public float revealDelay_s;

    public bool revealingCards;

    public bool hidingCards;
    public float hidingRate;

    // Use this for initialization
    void Start () {

        cardsCG = GameObject.Find("Panel_cards").GetComponent<CanvasGroup>();

        eye = GameObject.Find("eye2").GetComponent<Transform>();
        card1 = GameObject.Find("Card1").GetComponent<Transform>();
        card2 = GameObject.Find("Card2").GetComponent<Transform>();
        card3 = GameObject.Find("Card3").GetComponent<Transform>();

        eyeMat1 = eye.Find("IRIS").GetComponent<Renderer>();
        eyeMat2 = eye.Find("LID").GetComponent<Renderer>();

        HideCards(true);
        HideEye(true);
        revealing = false;

        revealingCards = false;
    }
	
	// Update is called once per frame
	void Update () {
	
        if (revealing)
        {
            if (revealDelayTimer > 0f)
            {
                revealDelayTimer -= Time.deltaTime;
                return;
            }
            /*
            if (cardsCG.alpha < 1f)
            {
                cardsCG.alpha += cardsRevealRate * Time.deltaTime;
            }
            else
            {
                revealing = false;
            }

            //eyeMat1.material.color = new Color(eyeMat1.material.color.r, eyeMat1.material.color.g, eyeMat1.material.color.b, 0f);
            if (eyeMat1.material.color.a < 1f)
            {
                eyeMat1.material.color = new Color(eyeMat1.material.color.r, eyeMat1.material.color.g, eyeMat1.material.color.b, eyeMat1.material.color.a + eyeRevealRate * Time.deltaTime);
            }*/
        }

        if (hidingCards)
        {
            if (cardsCG.alpha > 0f)
            {
                cardsCG.alpha -= hidingRate * Time.deltaTime;
            }
        }

        if (revealingCards)
        {
            if (cardsCG.alpha < 1f)
            {
                cardsCG.alpha += cardsRevealRate * Time.deltaTime;
            }
            else
            {
                revealingCards = false;
            }
        }
	}

    void HideEye(bool instant)
    {
        if (instant)
        {
            Color col1 = eyeMat1.material.color;
            eyeMat1.material.color = new Color(col1.r, col1.g, col1.b, 0f);
            Color col2 = eyeMat2.material.color;
            eyeMat2.material.color = new Color(col2.r, col2.g, col2.b, 0f);
            eye.gameObject.SetActive(false);
        }
        else
        {
            StartCoroutine(HideEyeSlowly());
        }

    }

    IEnumerator HideEyeSlowly()
    {
        while (eyeMat1.material.color.a > 0f)
        {
            eyeMat1.material.color = new Color(eyeMat1.material.color.r, eyeMat1.material.color.g, eyeMat1.material.color.b, eyeMat1.material.color.a - 0.05f);
            yield return new WaitForSeconds(0.02f);
        }
        eye.gameObject.SetActive(false);
    }

    public void HideCards(bool instant)
    {
        if (instant)
            cardsCG.alpha = 0f;
        else
            hidingCards = true;
    }

    void RevealOne()
    {
        card2.localPosition = card_pos_offscreen;
        card3.localPosition = card_pos_offscreen;

        card1.localPosition = card1_pos_1;
    }

    void RevealTwo()
    {
        card3.localPosition = card_pos_offscreen;

        card1.localPosition = card1_pos_2;
        card2.localPosition = card2_pos_2;
    }

    void RevealThree()
    {
        card1.localPosition = card1_pos_3;
        card2.localPosition = card2_pos_3;
        card3.localPosition = card3_pos_3;
    }

    public void Reveal(int cards)
    {
        revealing = true;
        eye.gameObject.SetActive(true);
        cardsCG.alpha = 0f;

        revealingCards = true;
        hidingCards = false;

        if (cards == 1)
            RevealOne();
        if (cards == 2)
            RevealTwo();
        if (cards == 3)
            RevealThree();
    }
}
