﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnController : MonoBehaviour {

    public enum TurnState
    {
        INFO_PHASE,
        SCAN_PHASE,
        DONE
    }

    public enum Rule
    {
        ONE_SURE,
        TWO_RELATION,
        THREE_COMPARISON,
        NO_RULE
    };

    public TurnState turnState;
    public Rule rule;

    public AudioSource audioPlayer;

    AudioClip sfx_turns_left, sfx_word_tries;
    AudioClip sfx_now_set, sfx_word_cards, sfx_over_my_eyes, sfx_truth;
    AudioClip sfx_last_turn1, sfx_last_turn2;
    public List<AudioClip> sfx_numbers;

    DataController dataController;
    CardController cardController;

    private int turnCount;
    private int turnsLeft;
    private int draw;
    private int previousDraw;

    public bool gameOn;

    public Transform cameraTrans;
    Vector3 camera_scan_pos;
    Vector3 camera_scan_rot;

    public float camToScanSpeed;
    public float camToScanRotSpeed;

    // Use this for initialization
    void Start () {

        camera_scan_pos = new Vector3(2f, 3f, -7.7f);
        camera_scan_rot = new Vector3(9.6f, 122.7f, 2f);

        audioPlayer = GetComponent<AudioSource>();

        dataController = GameObject.Find("MASTER").GetComponent<DataController>();
        cardController = GameObject.Find("Panel_cards").GetComponent<CardController>();
        cardController.gameObject.SetActive(false);

        sfx_numbers = new List<AudioClip>();
        sfx_numbers.Add(Resources.Load<AudioClip>("Audio/Numbers/1"));
        sfx_numbers.Add(Resources.Load<AudioClip>("Audio/Numbers/2"));
        sfx_numbers.Add(Resources.Load<AudioClip>("Audio/Numbers/3"));
        sfx_numbers.Add(Resources.Load<AudioClip>("Audio/Numbers/4"));
        sfx_numbers.Add(Resources.Load<AudioClip>("Audio/Numbers/5"));
        sfx_numbers.Add(Resources.Load<AudioClip>("Audio/Numbers/6"));
        sfx_numbers.Add(Resources.Load<AudioClip>("Audio/Numbers/7"));
        sfx_numbers.Add(Resources.Load<AudioClip>("Audio/Numbers/8"));
        sfx_numbers.Add(Resources.Load<AudioClip>("Audio/Numbers/9"));
        sfx_numbers.Add(Resources.Load<AudioClip>("Audio/Numbers/10"));
        sfx_numbers.Add(Resources.Load<AudioClip>("Audio/Numbers/11"));
        sfx_numbers.Add(Resources.Load<AudioClip>("Audio/Numbers/12"));

        sfx_turns_left = Resources.Load<AudioClip>("Audio/Turn_info/teilla_on_jaljella");
        sfx_word_tries = Resources.Load<AudioClip>("Audio/Turn_info/yritysta");
        sfx_now_set = Resources.Load<AudioClip>("Audio/Turn_info/nyt_asettakaa");
        sfx_word_cards = Resources.Load<AudioClip>("Audio/Turn_info/korttia");
        sfx_over_my_eyes = Resources.Load<AudioClip>("Audio/Turn_info/silmani_ylle");
        sfx_truth = Resources.Load<AudioClip>("Audio/Turn_info/totuus");

        sfx_last_turn1 = Resources.Load<AudioClip>("Audio/Turn_info/vikavuorokommentti1");
        sfx_last_turn2 = Resources.Load<AudioClip>("Audio/Turn_info/vikavuorokommentti2");

        gameOn = false;
        previousDraw = -1;
    }
	
	// Update is called once per frame
	void Update () {
		
        if (gameOn)
        {
            switch (turnState)
            {
                case TurnState.INFO_PHASE:
                    break;

                case TurnState.SCAN_PHASE:

                    cameraTrans.position = Vector3.MoveTowards(cameraTrans.position, camera_scan_pos, camToScanSpeed * Time.deltaTime);
                    cameraTrans.localRotation = Quaternion.Euler(Vector3.RotateTowards(cameraTrans.localRotation.eulerAngles, camera_scan_rot, camToScanRotSpeed, camToScanRotSpeed));

                    if (dataController.suspectsProcessed)
                    {
                        turnsLeft--;
                        if (turnsLeft > 0)
                        {
                            SetState(TurnState.INFO_PHASE);
                        }
                        else
                        {
                            SetState(TurnState.DONE);
                        }
                    }

                    break;
            }
        }

	}

    public void SetState(TurnState newState)
    {
        switch (newState)
        {
            case TurnState.INFO_PHASE:

                StartCoroutine(PlayInfoPhase());

                break;

            case TurnState.SCAN_PHASE:

                draw = Random.Range(0, 3);

                if (draw == previousDraw)
                {
                    if (draw < 2)
                        draw++;
                    else
                        draw = Random.Range(0, 1);
                }

                previousDraw = draw;

                if (draw == 0)
                    rule = Rule.ONE_SURE;
                if (draw == 1)
                    rule = Rule.TWO_RELATION;
                if (draw == 2)
                    rule = Rule.THREE_COMPARISON;

                cardController.gameObject.SetActive(true);
                if (turnsLeft == turnCount) /* Some delay for first reveal because of camera movement */
                {
                    cardController.revealDelayTimer = cardController.revealDelay_s;
                }
                cardController.Reveal(draw + 1);
                StartCoroutine(PlayScanPhase());
                dataController.StartRound((DataController.Rule)rule, draw + 1);

                break;
        }

        turnState = newState;
    }

    IEnumerator PlayInfoPhase()
    {
        audioPlayer.clip = sfx_turns_left;
        audioPlayer.Play();

        yield return new WaitForSeconds(audioPlayer.clip.length);

        audioPlayer.clip = sfx_numbers[turnsLeft-1];
        audioPlayer.Play();

        yield return new WaitForSeconds(audioPlayer.clip.length);

        audioPlayer.clip = sfx_word_tries;
        audioPlayer.Play();

        yield return new WaitForSeconds(audioPlayer.clip.length+1f);

        if (turnsLeft == 1)
        {
            int dice = Random.Range(0, 100);

            if (dice % 2 == 0)
                audioPlayer.clip = sfx_last_turn1;
            else
                audioPlayer.clip = sfx_last_turn2;

            audioPlayer.Play();

            yield return new WaitForSeconds(audioPlayer.clip.length + 1f);
        }

        SetState(TurnState.SCAN_PHASE);
    }

    IEnumerator PlayScanPhase()
    {
        audioPlayer.clip = sfx_now_set;
        audioPlayer.Play();

        yield return new WaitForSeconds(audioPlayer.clip.length);

        audioPlayer.clip = sfx_numbers[draw];
        audioPlayer.Play();

        yield return new WaitForSeconds(audioPlayer.clip.length);

        audioPlayer.clip = sfx_word_cards;
        audioPlayer.Play();

        yield return new WaitForSeconds(audioPlayer.clip.length);

        audioPlayer.clip = sfx_over_my_eyes;
        audioPlayer.Play();

        yield return new WaitForSeconds(audioPlayer.clip.length+1.5f);

        if (turnsLeft == turnCount) /* Only speak this on the first turn */
        {
            audioPlayer.clip = sfx_truth;
            audioPlayer.Play();

            yield return new WaitForSeconds(audioPlayer.clip.length);
        }
    }

    public void StartGame(int _turnCount)
    {
        turnCount = _turnCount;
        turnsLeft = turnCount;
        gameOn = true;
        SetState(TurnState.INFO_PHASE);
    }
}
