﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class NFC : MonoBehaviour {

    public enum Mode
    {
        IDLE,
        GAME,
        GUESS
    };

    public Mode mode;

	public Text tag_output_text;
	public bool tagFound;
    public bool tagReadSuccess;

    public float nfcRefreshRate_s;
    public float nfcWaitAfterScan_s;
    public float nfcRefresh_timer;
    public bool scanNfc;

	private AndroidJavaObject mActivity;
	private AndroidJavaObject mIntent;

    public DataController dataController;
    public GuessScript guessScript;
    public StateController stateController; 

	private string sAction;

    public int sector;
    public int block;

    public byte[] mifarePayload;


	void Start() {
		tag_output_text.text = "Scan a NFC tag...";
        dataController = GameObject.Find("MASTER").GetComponent<DataController>();
        guessScript = GameObject.Find("MASTER").GetComponent<GuessScript>();
        stateController = GameObject.Find("MASTER").GetComponent<StateController>();

        tagReadSuccess = false;
        tagFound = false;
        scanNfc = false;
        nfcRefresh_timer = nfcRefreshRate_s;
        mode = Mode.IDLE;
    }

	void Update() {

        if (mode == Mode.IDLE)
            return;

        if (nfcRefresh_timer <= 0f)
        {
            scanNfc = true;
            tagFound = false;
            nfcRefresh_timer = nfcRefreshRate_s;
        }
        else
        {
            scanNfc = false;
            nfcRefresh_timer -= Time.deltaTime;
        }

        if (!tagFound)
        {
            if (Input.GetKeyDown(KeyCode.Keypad1))
            {
                SendRead(sector, block, new byte[]{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 });
                return;
            }
            else if (Input.GetKeyDown(KeyCode.Keypad2))
            {
                SendRead(sector, block, new byte[] { 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 });
                return;
            }
            else if (Input.GetKeyDown(KeyCode.Keypad3))
            {
                SendRead(sector, block, new byte[] { 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 });
                return;
            }
            else if (Input.GetKeyDown(KeyCode.Keypad4))
            {
                SendRead(sector, block, new byte[] { 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 });
                return;
            }
            else if (Input.GetKeyDown(KeyCode.Keypad5))
            {
                SendRead(sector, block, new byte[] { 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 });
                return;
            }
        }

		if (Application.platform == RuntimePlatform.Android) {

			if (!tagFound && scanNfc) {

                try {
					// Create new NFC Android object
					mActivity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity"); // Activities open apps
					mIntent = mActivity.Call<AndroidJavaObject>("getIntent");
					sAction = mIntent.Call<String>("getAction"); // results are returned in the Intent object

                    /* Falls to TAG_DISCOVERED on some older phones  (OPPO N1 + Lollipop) */
                    if (sAction == "android.nfc.action.TECH_DISCOVERED" || sAction == "android.nfc.action.TAG_DISCOVERED")
                    {
                        if (sAction == "android.nfc.action.TECH_DISCOVERED")
                        {
                            Debug.Log("TECH_DISCOVERED");
                        }
                        if (sAction == "android.nfc.action.TAG_DISCOVERED")
                        {
                            Debug.Log("TAG_DISCOVERED");
                        }

                        AndroidJavaObject mifareInstance = new AndroidJavaClass("android.nfc.tech.MifareClassic").CallStatic<AndroidJavaObject>("get", mIntent.Call<AndroidJavaObject>("getParcelableExtra", "android.nfc.extra.TAG"));
                            
                        mifareInstance.Call("connect");
                        byte[] mifareKey = mifareInstance.GetStatic<byte[]>("KEY_DEFAULT");
                        
                        if (mifareInstance.Call<bool>("isConnected")) 
                        {
                            Debug.Log("TAG CONNECTED");
                            tag_output_text.text = "SECTOR " + sector + "\n";
                            if (mifareInstance.Call<bool>("authenticateSectorWithKeyA", sector, mifareKey))
                            {
                                mifarePayload = mifareInstance.Call<byte[]>("readBlock", block);

                                if (mifarePayload != null)
                                {
                                    tagReadSuccess = true;
                                    nfcRefresh_timer = nfcWaitAfterScan_s;
                                    SendRead(sector, block, mifarePayload);
                                }
                                else
                                {
                                    tagReadSuccess = false;
                                }

                                mifareInstance.Call("close");
                                mIntent.Call("removeExtra", "android.nfc.extra.TAG");
                                tag_output_text.text = "\n\nCLOSED TAG CONNECTION\n\nREMOVE CARD\n\n";
                                Debug.Log("CLOSED TAG CONNECTION. REMOVE CARD");
                            }
                        }
                        else
                        {
                            tag_output_text.text = "not connected\n";
                            Debug.Log("NOT CONNECTED");
                        }

						tagFound = true;
                        return;
					}
					else {
                        tagReadSuccess = false;
                        return;
					}
				}
				catch (Exception ex) {
					string text = ex.Message;
					tag_output_text.text = tag_output_text.text + "\n" + text;
                    tagFound = false;
                    tagReadSuccess = false;
                    if (mIntent != null)
                    {
                        mIntent.Call("removeExtra", "android.nfc.extra.TAG");
                    }
                }
			}
		}
	}

    void SendRead(int _sector, int _block, byte[] payload)
    {
        stateController.ReceiveRead(_sector, _block, payload);
    }
}