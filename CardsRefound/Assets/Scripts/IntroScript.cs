﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroScript : MonoBehaviour {

    public bool introFinished;

    AudioSource audioPlayer;
    AudioClip sfx_intro1, sfx_intro2, sfx_intro3, sfx_welcome;
    AudioClip sfx_boss, sfx_lackey, sfx_infiltrator, sfx_hacker, sfx_invader;
    AudioClip sfx_boss_name, sfx_sfx_lackey_name, sfx_infiltrator_name, sfx_hacker_name, sfx_invader_name;
    AudioClip sfx_in_room, sfx_word_also;
    List<AudioClip> sfx_roles;
    List<AudioClip> sfx_role_names;

    List<string> role_names = new List<string>{ "boss", "lackey", "infiltrator", "hacker", "invader" };
    List<string> rolePool;

    CanvasGroup introCG;
    Coroutine coroutine;

	// Use this for initialization
	void Start () {

        audioPlayer = GameObject.Find("MASTER").GetComponent<AudioSource>();

        sfx_intro1 = Resources.Load<AudioClip>("Audio/Intro/intro1");
        sfx_intro2 = Resources.Load<AudioClip>("Audio/Intro/intro2");
        sfx_intro3 = Resources.Load<AudioClip>("Audio/Intro/intro3");
        sfx_welcome = Resources.Load<AudioClip>("Audio/Intro/tervetuloa_murhahuoneeseen");

        sfx_roles = new List<AudioClip>();
        sfx_roles.Add(Resources.Load<AudioClip>("Audio/Roles/pomo_selostus"));
        sfx_roles.Add(Resources.Load<AudioClip>("Audio/Roles/alainen_selostus"));
        sfx_roles.Add(Resources.Load<AudioClip>("Audio/Roles/soluttautuja_selostus"));
        sfx_roles.Add(Resources.Load<AudioClip>("Audio/Roles/hakkeri_selostus"));
        sfx_roles.Add(Resources.Load<AudioClip>("Audio/Roles/murtautuja_selostus"));

        sfx_role_names = new List<AudioClip>();
        sfx_role_names.Add(Resources.Load<AudioClip>("Audio/Roles/pomo"));
        sfx_role_names.Add(Resources.Load<AudioClip>("Audio/Roles/alainen"));
        sfx_role_names.Add(Resources.Load<AudioClip>("Audio/Roles/soluttautuja"));
        sfx_role_names.Add(Resources.Load<AudioClip>("Audio/Roles/hakkeri"));
        sfx_role_names.Add(Resources.Load<AudioClip>("Audio/Roles/murtautuja"));

        sfx_in_room = Resources.Load<AudioClip>("Audio/Intro/talla_kertaa_huoneessa_on");
        sfx_word_also = Resources.Load<AudioClip>("Audio/Words/seka");

        introFinished = false;

        introCG = GetComponent<CanvasGroup>();
    }
	
	// Update is called once per frame
	void Update () {
	}

    IEnumerator PlayIntro()
    {
        int draw = Random.Range(0, 3);

        if (draw == 0)
        {
            audioPlayer.clip = sfx_intro1;
        }
        if (draw == 1)
        {
            audioPlayer.clip = sfx_intro2;
        }
        if (draw == 2)
        {
            audioPlayer.clip = sfx_intro3;
        }
        
        audioPlayer.Play();

        yield return new WaitForSeconds(audioPlayer.clip.length+1f);

        audioPlayer.clip = sfx_welcome;
        audioPlayer.Play();

        yield return new WaitForSeconds(audioPlayer.clip.length+1f);

        for (int i=0; i<5; i++)
        {
            audioPlayer.clip = sfx_roles[i];
            audioPlayer.Play();

            yield return new WaitForSeconds(audioPlayer.clip.length+0.5f);
        }

        StartCoroutine(PlayCharacters());
    }

    IEnumerator PlayCharacters()
    {
        introCG.alpha = 0f;
        introCG.interactable = false;

        yield return new WaitForSeconds(1.5f);    // Short pause

        audioPlayer.mute = false;
        audioPlayer.clip = sfx_in_room;
        audioPlayer.Play();

        yield return new WaitForSeconds(audioPlayer.clip.length+0.8f);

        for (int i = 0; i < rolePool.Count; i++)
        {
            if (i == rolePool.Count-1)  /* Play "and lastly" before the last role */
            {
                audioPlayer.clip = sfx_word_also;
                audioPlayer.Play();

                yield return new WaitForSeconds(audioPlayer.clip.length+0.4f);
            }

            audioPlayer.clip = sfx_role_names[role_names.IndexOf(rolePool[i])];
            audioPlayer.Play();

            yield return new WaitForSeconds(audioPlayer.clip.length+1f);
        }

        introFinished = true;
    }

    public void SkipIntro()
    {
        if (!introFinished)
        {
            audioPlayer.mute = true;
            StopCoroutine(coroutine);
            audioPlayer.Stop();
            StartCoroutine(PlayCharacters());
        }
    }

    public void HideIntro()
    {
        introCG.alpha = 0f;
        introCG.gameObject.SetActive(false);
    }

    public void StartIntro(List<string> gameRoles)
    {
        introCG.gameObject.SetActive(true);
        rolePool = gameRoles;
        coroutine = StartCoroutine(PlayIntro());
        introCG.alpha = 1f;
    }
}
