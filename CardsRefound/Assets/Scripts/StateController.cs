﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateController : MonoBehaviour {

    public struct GameData
    {
        public int playerCount;
        public int turns;
    }

    public enum GameState
    {
        MAIN_MENU,
        INTRO,
        GAME,
        GUESS,
        WIN,
        LOSE
    };

    public GameState gState;
    public GameData gData;

    NFC nfc;
    MenuController menuController;
    IntroScript introScript;
    TurnController turnController;
    DataController dataController;
    GuessScript guessScript;
    ResultScript resultScript;

    List<string> gameRolePool;
    public List<int> roleCardMappings;

    // Use this for initialization
    void Start () {

        Screen.sleepTimeout = SleepTimeout.NeverSleep;

        nfc = GetComponent<NFC>();
        menuController = GameObject.Find("Panel_menu").GetComponent<MenuController>();
        introScript = GameObject.Find("Panel_intro").GetComponent<IntroScript>();
        turnController = GetComponent<TurnController>();
        dataController = GameObject.Find("MASTER").GetComponent<DataController>();
        guessScript = GetComponent<GuessScript>();
        resultScript = GetComponent<ResultScript>();

        gameRolePool = new List<string>();
        roleCardMappings = new List<int>();

        SetState(GameState.MAIN_MENU);
	}
	
	// Update is called once per frame
	void Update () {

        if(Input.GetKeyDown(KeyCode.E))
        {
            SetState(GameState.WIN);
        }

        if (Input.GetKeyDown(KeyCode.G))
        {
            SetState(GameState.LOSE);
        }

        switch (gState)
        {
            case GameState.MAIN_MENU:

                if (menuController.menuFinished)
                {
                    gData.playerCount = menuController.playerCount;
                    gData.turns = menuController.turnCount;
                    menuController.HideMenu();
                    SetState(GameState.INTRO);
                }

                break;

            case GameState.INTRO:

                if (introScript.introFinished)
                {
                    Debug.Log("INTRO FINISHED");
                    SetState(GameState.GAME);
                }

                break;

            case GameState.GAME:

                if (turnController.turnState == TurnController.TurnState.DONE)
                {
                    Debug.Log("PLAYTIME IS OVER");
                    SetState(GameState.GUESS);
                }

                break;

            case GameState.GUESS:

                if (guessScript.guessingDone)
                {
                    if (guessScript.guessedRight)
                    {
                        SetState(GameState.WIN);
                    }
                    else
                    {
                        SetState(GameState.LOSE);
                    }
                }

                break;
        }
		
	}

    void SetState(GameState newState)
    {
        switch (newState)
        {
            case GameState.MAIN_MENU:

                introScript.HideIntro();
                nfc.mode = NFC.Mode.IDLE;

                break;

            case GameState.INTRO:

                PopulateRolePool();
                for (int i=0; i<gameRolePool.Count; i++)
                {
                    roleCardMappings.Add(255);
                }
                introScript.StartIntro(gameRolePool);

                break;

            case GameState.GAME:

                turnController.StartGame(gData.turns);
                nfc.mode = NFC.Mode.GAME;

                break;

            case GameState.GUESS:

                guessScript.StartGuessing(gameRolePool);
                nfc.mode = NFC.Mode.GUESS;

                break;

            case GameState.WIN:

                resultScript.SetState(ResultScript.ResultState.PLAY_WIN);

                break;

            case GameState.LOSE:

                resultScript.SetState(ResultScript.ResultState.PLAY_LOSE);

                break;
        }

        gState = newState;
    }

    public void ReceiveRead(int sector, int block, byte[] payload)
    {
        byte card_num = payload[0];
        Debug.Log("CHARACTER = " + card_num);

        int match = roleCardMappings.IndexOf(card_num);

        if (match == -1)
        {
            MapNewCard(card_num);
        }

        int character_map_i = roleCardMappings.IndexOf(card_num);
        string character = gameRolePool[character_map_i];

        if (gState == GameState.GAME)
        {
            dataController.ReceiveRead(sector, block, character);
        }
        if (gState == GameState.GUESS)
        {
            guessScript.ReceiveGuessRead(sector, block, character);
        }
    }

    void MapNewCard(int card_num)
    {
        List<int> emptySlots = new List<int>();

        for (int i=0; i<roleCardMappings.Count; i++)
        {
            if (roleCardMappings[i] == 255)
                emptySlots.Add(i);
        }

        if (emptySlots.Count > 0)   /* Map new card to a random empty slot */
        {
            int draw = Random.Range(0, emptySlots.Count);
            roleCardMappings[emptySlots[draw]] = card_num;
        }
        else
        {
            roleCardMappings[Random.Range(0, roleCardMappings.Count)] = card_num;   /* Place the first card freely in map */
        }
    }

    void PopulateRolePool()
    {
        gameRolePool.Add("boss");

        switch (gData.playerCount)
        {
            case 4:
                Draw4PlayerGame();
                break;
            case 5:
                Draw5PlayerGame();
                break;
            case 6:
                Draw6PlayerGame();
                break;
            case 7:
                Draw7PlayerGame();
                break;
            case 8:
                Draw8PlayerGame();
                break;
        }
    }

    void Draw4PlayerGame()
    {
        int draw = Random.Range(0,3);

        switch (draw)
        {
            case 0:
                gameRolePool.Add("hacker");
                gameRolePool.Add("lackey");
                gameRolePool.Add("lackey");
                break;

            case 1:
                gameRolePool.Add("hacker");
                gameRolePool.Add("invader");
                gameRolePool.Add("lackey");
                break;

            case 2:
                gameRolePool.Add("lackey");
                gameRolePool.Add("lackey");
                gameRolePool.Add("lackey");
                break;
        }
    }

    void Draw5PlayerGame()
    {
        int draw = Random.Range(0, 5);

        switch (draw)
        {
            case 0:
                gameRolePool.Add("hacker");
                gameRolePool.Add("lackey");
                gameRolePool.Add("lackey");
                gameRolePool.Add("lackey");
                break;

            case 1:
                gameRolePool.Add("hacker");
                gameRolePool.Add("invader");
                gameRolePool.Add("lackey");
                gameRolePool.Add("lackey");
                break;

            case 2:
                gameRolePool.Add("infiltrator");
                gameRolePool.Add("lackey");
                gameRolePool.Add("lackey");
                gameRolePool.Add("lackey");
                break;

            case 3:
                gameRolePool.Add("infiltrator");
                gameRolePool.Add("hacker");
                gameRolePool.Add("lackey");
                gameRolePool.Add("lackey");
                break;

            case 4:
                gameRolePool.Add("infiltrator");
                gameRolePool.Add("hacker");
                gameRolePool.Add("invader");
                gameRolePool.Add("lackey");
                break;
        }
    }

    void Draw6PlayerGame()
    {
        Draw5PlayerGame();

        if (gameRolePool.IndexOf("infiltrator") == -1) /* Add an infiltrator if there is not already one */
        {
            gameRolePool.Add("infiltrator");
        }
        else if (gameRolePool.IndexOf("hacker") == -1)
        {
            gameRolePool.Add("hacker");
        }
        else if (gameRolePool.IndexOf("invader") == -1)
        {
            gameRolePool.Add("invader");
        }
        else
        {
            gameRolePool.Add("lackey");
        }
    }

    void Draw7PlayerGame()
    {
        Draw6PlayerGame();
        gameRolePool.Add("invader");
    }

    void Draw8PlayerGame()
    {
        Draw7PlayerGame();
        gameRolePool.Add("lackey");
    }
}
