﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SfxController : MonoBehaviour {

    AudioSource audioPlayer;
    AudioClip sfx_water_drop;

    public float next_sfx_timer;
    public float min_play_interval;
    public float max_play_interval;

    // Use this for initialization
    void Start () {
        audioPlayer = GetComponent<AudioSource>();
        sfx_water_drop = Resources.Load<AudioClip>("Audio/Licensed/Spooky_Water_drops");
        audioPlayer.clip = sfx_water_drop;

        ShuffleTimer(audioPlayer.clip.length);
	}
	
	// Update is called once per frame
	void Update () {

        next_sfx_timer -= Time.deltaTime;
		
        if (next_sfx_timer <= 0f)
        {
            audioPlayer.clip = sfx_water_drop;
            audioPlayer.Play();

            ShuffleTimer(audioPlayer.clip.length);
        }
	}

    void ShuffleTimer(float clip_len)
    {
        next_sfx_timer = Random.Range(min_play_interval + clip_len, max_play_interval + clip_len);
    }
}
