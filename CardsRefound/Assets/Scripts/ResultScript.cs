﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ResultScript : MonoBehaviour {

    public enum ResultState
    {
        IDLE,
        PLAY_WIN,
        PLAY_LOSE
    };

    ResultState state;

    AudioSource audioPlayer;
    AudioClip sfx_correct, sfx_freedom, sfx_incorrect, sfx_goodbye;

    public Light goodbyeLight;
    public Light winLight;

    public GameObject panel_black;

    public Transform cameraTrans;
    Transform trapPlate;
    Transform trapSpikes;
    Transform trapLookPoint;
    Transform doorTrans;

    Vector3 cameraDoorPos = new Vector3(-1.5f, 3.7f, -5.75f);
    Vector3 cameraZoomPos = new Vector3(-6.7f, 4f, 1.1f);
    Vector3 cameraStartPos;

    public float trapSpeed;
    public float trapShakeRate;
    public float trapShakeMagnitude;

    bool shakeTrap;
    bool dropTrap;
    bool lookUp;
    bool openDoor;

    bool moveCamera;
    bool rotateCamera;
    bool zoomIn;
    public float camRotRate;
    public float camMoveSpeed;
    public float camRotCounter;
    public float doorSpeed;
    public float lightUpRate;

    float camMoveStartTime;
    float camMoveDist;

	// Use this for initialization
	void Start () {

        state = ResultState.IDLE;
        audioPlayer = GetComponent<AudioSource>();

        goodbyeLight.enabled = false;
        winLight.enabled = false;

        sfx_correct = Resources.Load<AudioClip>("Audio/Ending/oikein");
        sfx_freedom = Resources.Load<AudioClip>("Audio/Ending/vapaiksi");
        sfx_incorrect = Resources.Load<AudioClip>("Audio/Ending/vaarin");
        sfx_goodbye = Resources.Load<AudioClip>("Audio/Ending/hyvasti");

        trapPlate = GameObject.Find("TRAP_PLATE").GetComponent<Transform>();
        trapSpikes = GameObject.Find("TRAP_SPIKES").GetComponent<Transform>();
        trapLookPoint = GameObject.Find("TrapLookPoint").GetComponent<Transform>();
        doorTrans = GameObject.Find("DoorPoint").GetComponent<Transform>();

        moveCamera = false;
        rotateCamera = false;
        shakeTrap = false;
        dropTrap = false;
        lookUp = false;
        openDoor = false;
        zoomIn = false;
        camRotCounter = 0f;

        trapSpikes.SetParent(trapPlate, true);
    }
	
	// Update is called once per frame
	void Update () {
		
        switch (state)
        {
            case ResultState.IDLE:
                break;

            case ResultState.PLAY_WIN:

                break;

            case ResultState.PLAY_LOSE:

                break;
        }

        if (rotateCamera && Mathf.Abs(camRotCounter) < 180f && !lookUp)
        {
            float increment = camRotRate * Time.deltaTime;
            cameraTrans.Rotate(Vector3.up, increment);
            camRotCounter += increment;
        }

        if (moveCamera)
        {
            float distTraveled = (Time.time - camMoveStartTime) * camMoveSpeed;
            float fractionTraveled = distTraveled / camMoveDist;

            cameraTrans.position = Vector3.Lerp(cameraStartPos, cameraDoorPos, fractionTraveled);
        }

        if (zoomIn)
        {
            float distTraveled = (Time.time - camMoveStartTime) * camMoveSpeed * 6;
            float fractionTraveled = distTraveled / camMoveDist;

            cameraTrans.position = Vector3.Lerp(cameraStartPos, cameraZoomPos, fractionTraveled);
        }

        if (shakeTrap)
        {
            trapPlate.localPosition += ((Vector3.right * Mathf.Cos(Time.time * 1.5f * trapShakeRate)) + (Vector3.left * Mathf.Sin(Time.time * trapShakeRate))) * trapShakeMagnitude;
        }

        if (lookUp)
        {
            cameraTrans.rotation = Quaternion.RotateTowards(cameraTrans.rotation, Quaternion.LookRotation(trapLookPoint.position - cameraTrans.position), 2f);
        }

        if (dropTrap)
        {
            trapPlate.localPosition += Vector3.up * -trapSpeed;
            if (trapPlate.localPosition.y <= 3f)
            {
                panel_black.SetActive(true);
                dropTrap = false;
            }
        }

        if (openDoor)
        {
            if (doorTrans.localRotation.y < 260f)
            {
                doorTrans.Rotate(Vector3.up, doorSpeed * Time.deltaTime);
            }
            winLight.intensity += lightUpRate * Time.deltaTime;
        }
	}

    public void SetState(ResultState newState)
    {
        rotateCamera = true;

        switch (newState)
        {
            case ResultState.PLAY_WIN:

                StartCoroutine(PlayWin());

                break;

            case ResultState.PLAY_LOSE:

                StartCoroutine(PlayLose());

                break;
        }
    }

    IEnumerator PlayWin()
    {
        yield return new WaitForSeconds(3f);

        cameraStartPos = cameraTrans.localPosition;
        camMoveDist = Vector3.Distance(cameraStartPos, cameraDoorPos);
        camMoveStartTime = Time.time;
        moveCamera = true;

        yield return new WaitForSeconds(4.5f);

        audioPlayer.clip = sfx_correct;
        audioPlayer.Play();
        yield return new WaitForSeconds(audioPlayer.clip.length);

        openDoor = true;
        winLight.enabled = true;

        audioPlayer.clip = sfx_freedom;
        audioPlayer.Play();
        yield return new WaitForSeconds(audioPlayer.clip.length+0.5f);

        moveCamera = false;
        zoomIn = true;
        cameraStartPos = cameraTrans.localPosition;
        camMoveDist = Vector3.Distance(cameraStartPos, cameraZoomPos);
        camMoveStartTime = Time.time;

        yield return new WaitForSeconds(1.7f);

        SceneManager.LoadScene("Game", LoadSceneMode.Single);
    }

    IEnumerator PlayLose()
    {
        yield return new WaitForSeconds(3f);

        cameraStartPos = cameraTrans.localPosition;
        camMoveDist = Vector3.Distance(cameraStartPos, cameraDoorPos);
        camMoveStartTime = Time.time;
        moveCamera = true;

        yield return new WaitForSeconds(3f);

        goodbyeLight.enabled = true;

        audioPlayer.clip = sfx_incorrect;
        audioPlayer.Play();
        yield return new WaitForSeconds(audioPlayer.clip.length+0.5f);

        lookUp = true;

        audioPlayer.clip = sfx_goodbye;
        audioPlayer.Play();
        yield return new WaitForSeconds(audioPlayer.clip.length / 2);

        shakeTrap = true;

        yield return new WaitForSeconds(audioPlayer.clip.length / 2 +1f);

        dropTrap = true;

        yield return new WaitForSeconds(4f);

        SceneManager.LoadScene("Game", LoadSceneMode.Single);
    }
}
