﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DataController : MonoBehaviour {

    public struct Character
    {
        public string name;
        public int num;
        public AudioClip audio;
        public int rank;

        public Character(string _name, int _num, AudioClip _audio, int _rank)
        {
            name = _name;
            num = _num;
            audio = _audio;
            rank = _rank;
        }
    };

    public enum State
    {
        IDLE,
        RECEIVING,
        PROCESS_SUSPECTS
    };

    public enum Rule
    {
        ONE_SURE,
        TWO_RELATION,
        THREE_COMPARISON
    };

    Character boss;
    Character lackey;
    Character infiltrator;
    Character hacker;
    Character invader;

    public Image card1_img, card2_img, card3_img;

    public List<Character> inspectionPool;

    List<List<AudioClip>> sfx_cluePool;
    List<AudioClip> sfx_relations;
    List<AudioClip> sfx_comparisons;
    List<AudioClip> sfx_words;

    AudioClip sfx_ding;

    AudioSource audioPlayer;
    public State activeState;
    public Rule activeRule;

    public int player_count;
    public bool flag_suspectAdded;

    public int cardsRead;
    public int cardsRequired;
    public bool suspectsProcessed;

    CardController cardController;

    // Use this for initialization
    void Start () {

        audioPlayer = gameObject.GetComponent<AudioSource>();

        boss = new Character("boss", 1, Resources.Load<AudioClip>("Audio/Roles/pomo"), 1);
        lackey = new Character("lackey", 2, Resources.Load<AudioClip>("Audio/Roles/alainen"), 3);
        infiltrator = new Character("infiltrator", 3, Resources.Load<AudioClip>("Audio/Roles/soluttautuja"), 2);
        hacker = new Character("hacker", 4, Resources.Load<AudioClip>("Audio/Roles/hakkeri"), 2);
        invader = new Character("invader", 5, Resources.Load<AudioClip>("Audio/Roles/murtautuja"), 2);

        inspectionPool = new List<Character>();
        sfx_relations = new List<AudioClip>();
        sfx_comparisons = new List<AudioClip>();
        sfx_cluePool = new List<List<AudioClip>>();
        sfx_words = new List<AudioClip>();

        sfx_relations.Add(Resources.Load<AudioClip>("Audio/Relations/ensimmainen_kusessa_jos_toinen_mohlii"));
        sfx_relations.Add(Resources.Load<AudioClip>("Audio/Relations/ensimmainen_tarvitsee_toista"));
        sfx_relations.Add(Resources.Load<AudioClip>("Audio/Relations/kaksi_voi_olla"));
        sfx_relations.Add(Resources.Load<AudioClip>("Audio/Relations/naista_kahdesta"));
        sfx_relations.Add(Resources.Load<AudioClip>("Audio/Relations/naista_kolmesta"));
        sfx_relations.Add(Resources.Load<AudioClip>("Audio/Relations/nama_kaksi_ovat_tasaveroisia"));
        sfx_relations.Add(Resources.Load<AudioClip>("Audio/Relations/on_itse_vastuussa_onnistumisestaan"));
        sfx_relations.Add(Resources.Load<AudioClip>("Audio/Relations/tama_henkilo_on_varmasti"));
        sfx_relations.Add(Resources.Load<AudioClip>("Audio/Relations/tarvitsee_kaikkia_muita_onnistuakseen"));
        sfx_relations.Add(Resources.Load<AudioClip>("Audio/Relations/toinen_tarvitsee_ensimmaista"));
        sfx_relations.Add(Resources.Load<AudioClip>("Audio/Relations/toinen_voi_olla"));

        sfx_comparisons.Add(Resources.Load<AudioClip>("Audio/Comparisons/on_alempiarvoinen_kuin"));
        sfx_comparisons.Add(Resources.Load<AudioClip>("Audio/Comparisons/on_alhaisin_kaikista"));
        sfx_comparisons.Add(Resources.Load<AudioClip>("Audio/Comparisons/on_kaikista_ylimpana"));
        sfx_comparisons.Add(Resources.Load<AudioClip>("Audio/Comparisons/on_keskenaan_eriarvoisia"));
        sfx_comparisons.Add(Resources.Load<AudioClip>("Audio/Comparisons/on_keskenaan_tasaarvoisia"));
        sfx_comparisons.Add(Resources.Load<AudioClip>("Audio/Comparisons/on_ylempiarvoinen_kuin"));

        sfx_words.Add(Resources.Load<AudioClip>("Audio/Words/ensimmainen"));
        sfx_words.Add(Resources.Load<AudioClip>("Audio/Words/toinen"));
        sfx_words.Add(Resources.Load<AudioClip>("Audio/Words/kolmas"));
        sfx_words.Add(Resources.Load<AudioClip>("Audio/Words/ja"));
        sfx_words.Add(Resources.Load<AudioClip>("Audio/Words/pelaajaa"));
        sfx_words.Add(Resources.Load<AudioClip>("Audio/Words/seka"));

        sfx_ding = Resources.Load<AudioClip>("Audio/Effects/Ding");

        cardController = GameObject.Find("Panel_cards").GetComponent<CardController>();

        activeRule = Rule.TWO_RELATION;
        activeState = State.IDLE;

        flag_suspectAdded = false;
    }
	
	// Update is called once per frame
	void Update () {
		
        switch (activeState)
        {
            case State.IDLE:
                break;

            case State.RECEIVING:

                if (flag_suspectAdded)
                {
                    flag_suspectAdded = false;
                    CheckPool();
                    cardsRead++;
                }

                break;

            case State.PROCESS_SUSPECTS:

                Debug.Log("PROCESSING SUSPECTS NOW...");
                cardController.HideCards(false);
                ProcessSuspects();
                inspectionPool.Clear();
                activeState = State.IDLE;

                break;
        }
	}

    public void ProcessSuspects()
    {
        sfx_cluePool.Clear();

        for (int i=0; i<inspectionPool.Count; i++)
        {
            Debug.Log("INSPECTION POOL(" + i + "): " + inspectionPool[i].name);
        }

        switch (activeRule)
        {
            case Rule.ONE_SURE:
                SayCharacter(inspectionPool[0]);
                break;

            case Rule.TWO_RELATION:

                SayRelation2(inspectionPool[0], inspectionPool[1]);

                break;

            case Rule.THREE_COMPARISON:

                SayComparison3(inspectionPool[0], inspectionPool[1], inspectionPool[2]);

                break;
        }
    }

    public void SayCharacter(Character character)
    {
        sfx_cluePool.Add(new List<AudioClip> { sfx_relations[7], character.audio });
        SayClues();
    }

    public void SayRelation2(Character first, Character second)
    {
        /* BOSS CLUES */

        if (first.name == "boss" || second.name == "boss")
        {
            if (first.name == "boss")
            {
                sfx_cluePool.Add(new List<AudioClip> { sfx_words[0], sfx_relations[8] });
                sfx_cluePool.Add(new List<AudioClip> { sfx_relations[0] });
                sfx_cluePool.Add(new List<AudioClip> { sfx_relations[1] });

                if (second.name == "lackey" || second.name == "hacker")
                {
                    sfx_cluePool.Add(new List<AudioClip> { sfx_words[1], sfx_relations[6] });
                }
            }

            if (second.name == "boss")
            {
                sfx_cluePool.Add(new List<AudioClip> { sfx_words[1], sfx_relations[8] });

                sfx_cluePool.Add(new List<AudioClip> { sfx_relations[9] });

                if (first.name == "lackey" || first.name == "hacker")
                {
                    sfx_cluePool.Add(new List<AudioClip> { sfx_words[0], sfx_relations[6] });
                }

            }

            SayClues();
            return;
        }


        /* INFILTRATOR */

        if (first.name == "infiltrator" || second.name == "infiltrator")
        {
            if (first.name == "infiltrator")
            {
                sfx_cluePool.Add(new List<AudioClip> { sfx_words[0], sfx_relations[8] });

                sfx_cluePool.Add(new List<AudioClip> { sfx_relations[0] });
                sfx_cluePool.Add(new List<AudioClip> { sfx_relations[1] });

                if (second.name == "lackey" || second.name == "hacker")
                {
                    sfx_cluePool.Add(new List<AudioClip> { sfx_words[1], sfx_relations[6] });
                }
            }

            if (second.name == "infiltrator")
            {
                sfx_cluePool.Add(new List<AudioClip> { sfx_words[1], sfx_relations[8] });

                sfx_cluePool.Add(new List<AudioClip> { sfx_relations[9] });

                if (first.name == "lackey" || first.name == "hacker")
                {
                    sfx_cluePool.Add(new List<AudioClip> { sfx_words[0], sfx_relations[6] });
                }

            }

            SayClues();
            return;
        }

        /* INVADER CLUES */

        if (first.name == "invader" || second.name == "invader")
        {
            if (first.name == "invader")
            {
                if (second.name == "invader")
                {
                    sfx_cluePool.Add(new List<AudioClip> { sfx_relations[5] });
                    SayClues();
                    return;
                }

                if (second.name == "lackey")
                {
                    sfx_cluePool.Add(new List<AudioClip> { sfx_words[1], sfx_relations[6] });
                }

                if (second.name == "hacker")
                {
                    sfx_cluePool.Add(new List<AudioClip> { sfx_words[1], sfx_relations[6] });
                    sfx_cluePool.Add(new List<AudioClip> { sfx_relations[0] });
                    sfx_cluePool.Add(new List<AudioClip> { sfx_relations[1] });
                }
            }

            if (second.name == "invader")
            {
                if (first.name == "lackey")
                {
                    sfx_cluePool.Add(new List<AudioClip> { sfx_words[0], sfx_relations[6] });
                }

                if (first.name == "hacker")
                {
                    sfx_cluePool.Add(new List<AudioClip> { sfx_words[0], sfx_relations[6] });
                    sfx_cluePool.Add(new List<AudioClip> { sfx_relations[9] });
                }
            }

            SayClues();
            return;
        }


        /* LACKEY & HACKER CLUES */

        sfx_cluePool.Add(new List<AudioClip> { sfx_words[0], sfx_relations[6] });
        sfx_cluePool.Add(new List<AudioClip> { sfx_words[1], sfx_relations[6] });

        if (first.name == second.name)
        {
            sfx_cluePool.Add(new List<AudioClip> { sfx_relations[5] });
        }

        SayClues();
    }

    public void SayComparison3(Character first, Character second, Character third)
    {
        int rank1 = first.rank;
        int rank2 = second.rank;
        int rank3 = third.rank;
        int match_i = -1;

        List<int> ranks = new List<int> { rank1, rank2, rank3 };

        match_i = ranks.IndexOf(boss.rank); /* Is there a boss? */
        if (match_i != -1)
        {
            sfx_cluePool.Add( new List<AudioClip> { sfx_relations[4], sfx_words[match_i], sfx_comparisons[2] });

            /* Boss is always higher than the other two */
            if (match_i == 0)
            {
                sfx_cluePool.Add(new List<AudioClip> { sfx_relations[4], sfx_words[match_i], sfx_comparisons[5], sfx_words[1] });
                sfx_cluePool.Add(new List<AudioClip> { sfx_relations[4], sfx_words[match_i], sfx_comparisons[5], sfx_words[2] });
            }
            if (match_i == 1)
            {
                sfx_cluePool.Add(new List<AudioClip> { sfx_relations[4], sfx_words[match_i], sfx_comparisons[5], sfx_words[0] });
                sfx_cluePool.Add(new List<AudioClip> { sfx_relations[4], sfx_words[match_i], sfx_comparisons[5], sfx_words[2] });
            }
            if (match_i == 2)
            {
                sfx_cluePool.Add(new List<AudioClip> { sfx_relations[4], sfx_words[match_i], sfx_comparisons[5], sfx_words[0] });
                sfx_cluePool.Add(new List<AudioClip> { sfx_relations[4], sfx_words[match_i], sfx_comparisons[5], sfx_words[1] });
            }
        }

        if (rank1 == 3 && rank2 < 3 && rank3 < 3)
        {
            sfx_cluePool.Add(new List<AudioClip> { sfx_words[0], sfx_comparisons[1] }); /* Generally lowest rank */

            if (rank2 < 3 && rank3 < 3)
            {
                sfx_cluePool.Add(new List<AudioClip> { sfx_relations[4], sfx_words[0], sfx_comparisons[1] }); /* lowest of all three */
            }
        }
        if (rank2 == 3 && rank1 < 3 && rank3 < 3)
        {
            sfx_cluePool.Add(new List<AudioClip> { sfx_words[1], sfx_comparisons[1] }); /* Generally lowest rank */

            if (rank1 < 3 && rank3 < 3)
            {
                sfx_cluePool.Add(new List<AudioClip> { sfx_relations[4], sfx_words[1], sfx_comparisons[1] }); /* lowest of all three */
            }
        }
        if (rank3 == 3 && rank1 < 3 && rank2 < 3)
        {
            sfx_cluePool.Add(new List<AudioClip> { sfx_words[2], sfx_comparisons[1] }); /* Generally lowest rank */

            if (rank1 < 3 && rank2 < 3)
            {
                sfx_cluePool.Add(new List<AudioClip> { sfx_relations[4], sfx_words[2], sfx_comparisons[1] }); /* lowest of all three */
            }
        }

        if (rank1 < rank2)
        {
            sfx_cluePool.Add(new List<AudioClip> { sfx_words[0], sfx_comparisons[0], sfx_words[1] });
            sfx_cluePool.Add(new List<AudioClip> { sfx_words[1], sfx_comparisons[5], sfx_words[0] });
        }
        if (rank1 < rank3)
        {
            sfx_cluePool.Add(new List<AudioClip> { sfx_words[0], sfx_comparisons[0], sfx_words[2] });
            sfx_cluePool.Add(new List<AudioClip> { sfx_words[2], sfx_comparisons[5], sfx_words[0] });
        }
        if (rank2 < rank1)
        {
            sfx_cluePool.Add(new List<AudioClip> { sfx_words[1], sfx_comparisons[0], sfx_words[0] });
            sfx_cluePool.Add(new List<AudioClip> { sfx_words[0], sfx_comparisons[5], sfx_words[1] });
        }
        if (rank2 < rank3)
        {
            sfx_cluePool.Add(new List<AudioClip> { sfx_words[1], sfx_comparisons[0], sfx_words[2] });
            sfx_cluePool.Add(new List<AudioClip> { sfx_words[2], sfx_comparisons[5], sfx_words[1] });
        }
        if (rank3 < rank1)
        {
            sfx_cluePool.Add(new List<AudioClip> { sfx_words[2], sfx_comparisons[0], sfx_words[0] });
            sfx_cluePool.Add(new List<AudioClip> { sfx_words[0], sfx_comparisons[5], sfx_words[2] });
        }
        if (rank3 < rank2)
        {
            sfx_cluePool.Add(new List<AudioClip> { sfx_words[2], sfx_comparisons[0], sfx_words[1] });
            sfx_cluePool.Add(new List<AudioClip> { sfx_words[1], sfx_comparisons[5], sfx_words[2] });
        }

        /* EQUAL RANKS */
        if (rank1 == rank2)
        {
            sfx_cluePool.Add(new List<AudioClip> { sfx_words[0], sfx_words[3], sfx_words[1], sfx_comparisons[4] });
        }
        if (rank1 == rank3)
        {
            sfx_cluePool.Add(new List<AudioClip> { sfx_words[0], sfx_words[3], sfx_words[2], sfx_comparisons[4] });
        }
        if (rank2 == rank3)
        {
            sfx_cluePool.Add(new List<AudioClip> { sfx_words[1], sfx_words[3], sfx_words[2], sfx_comparisons[4] });
        }

        /* DIFFERENT RANKS */
        if (rank1 != rank2)
        {
            sfx_cluePool.Add(new List<AudioClip> { sfx_words[0], sfx_words[3], sfx_words[1], sfx_comparisons[3] });
        }
        if (rank1 != rank3)
        {
            sfx_cluePool.Add(new List<AudioClip> { sfx_words[0], sfx_words[3], sfx_words[2], sfx_comparisons[3] });
        }
        if (rank2 != rank3)
        {
            sfx_cluePool.Add(new List<AudioClip> { sfx_words[1], sfx_words[3], sfx_words[2], sfx_comparisons[3] });
        }

        SayClues();
    }

    public void SayClues()
    {
        int clue = Random.Range(0, sfx_cluePool.Count);

        StartCoroutine(playClueSequence(sfx_cluePool[clue]));
    }

    IEnumerator playClueSequence(List<AudioClip> clue_seq)
    {
        for (int sfx_i = 0; sfx_i < clue_seq.Count; sfx_i++)
        {
            // Debug.Log("CLUE SFX :  " + clue_seq[sfx_i].name);
            audioPlayer.clip = clue_seq[sfx_i];
            audioPlayer.Play();

            yield return new WaitForSeconds(audioPlayer.clip.length);
        }

        yield return new WaitForSeconds(2f);

        suspectsProcessed = true;
    }

    public void CheckPool()
    {
        int suspects = inspectionPool.Count;
        Debug.Log("SUSPECTS IN POOL = " + suspects);

        if (suspects == 1)
            card1_img.color = Color.green;

        if (suspects == 2)
            card2_img.color = Color.green;

        if (suspects == 3)
            card3_img.color = Color.green;

        switch (activeRule)
        {
            case Rule.ONE_SURE:

                if (suspects == 1)
                {
                    // Debug.Log("ALL SUSPECTS ADDED (1) (" + inspectionPool[0].name + ")");
                    activeState = State.PROCESS_SUSPECTS;
                }

                break;

            case Rule.TWO_RELATION:

                if (suspects == 2)
                {
                    //Debug.Log("ALL SUSPECTS ADDED (2)");
                    activeState = State.PROCESS_SUSPECTS;
                }
                else
                {
                    audioPlayer.PlayOneShot(sfx_ding);
                }

                break;

            case Rule.THREE_COMPARISON:

                if (suspects == 3)
                {
                    //Debug.Log("ALL SUSPECTS ADDED (3)");
                    activeState = State.PROCESS_SUSPECTS;
                }
                else
                {
                    audioPlayer.PlayOneShot(sfx_ding);
                }

                break;
        }
    }

    public void ReceiveRead(int sector, int block, string character)
    {
        Debug.Log("CHARACTER = " + character);

        if (character == "boss")
        {
            AddSuspect(boss);
        }
        if (character == "lackey")
        {
            AddSuspect(lackey);
        }
        if (character == "infiltrator")
        {
            AddSuspect(infiltrator);
        }
        if (character == "hacker")
        {
            AddSuspect(hacker);
        }
        if (character == "invader")
        {
            AddSuspect(invader);
        }
    }

    public void AddSuspect(Character character)
    {
        if (activeState != State.RECEIVING)
            return;

        inspectionPool.Add(character);
        //Debug.Log("ADDED SUSPECT " + character.name);
        flag_suspectAdded = true;
    }

    public void StartRound(Rule _rule, int _cardsRequired)
    {
        activeRule = _rule;
        cardsRead = 0;
        cardsRequired = _cardsRequired;
        suspectsProcessed = false;
        activeState = State.RECEIVING;
        card1_img.color = Color.white;
        card2_img.color = Color.white;
        card3_img.color = Color.white;
    }
}
