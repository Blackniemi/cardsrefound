# CardsRefound

A glorius mix of NFC card games packed into a single game! Warms the heart of any party!


## BETA TODO

### Critical
* Bug: with 3 card rule, saying rank relation of 2 cards may be in inverse order

### Major
* Test difficulty balancing - adjust turn counts
* Test with different phone models - improve compability
  -> Juho's Sony Experia <?> detects NFC only on Android level, but not on the game app
* Optimize wordings (too time-consuming, informative enough?)
* Credit texts to the side of the main menu (on the left wall?)

### Minor 
* Tune camera runs
* "Pian saatte tietää niiden totuuden" is grammatically incorrect and is said if the first rule is single card
* Test fancy post processing effects
* Eye particles are colored on Juho's phone, when they should be pure white
